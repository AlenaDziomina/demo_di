package com.learning.didemo.controller;

import com.learning.didemo.service.impl.CustomGreetingService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PropertyInjectorControllerTest {

    PropertyInjectorController controller;

    @BeforeEach
    void setUp() {
        controller = new PropertyInjectorController();
        controller.greetingService = new CustomGreetingService();
    }

    @Test
    void sayHello() {
        String result = controller.sayHello();
        System.out.println(result);
        assert "Hello world".equals(result);
    }
}