package com.learning.didemo.controller;

import com.learning.didemo.service.GreetingService;

public class TestGreetingService implements GreetingService {
    @Override
    public String getGreeting() {
        return "test";
    }
}
