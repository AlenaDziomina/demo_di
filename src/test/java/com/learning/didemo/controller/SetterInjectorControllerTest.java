package com.learning.didemo.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SetterInjectorControllerTest {

    private SetterInjectorController controller;

    @BeforeEach
    void setUp() {
        controller = new SetterInjectorController();
        controller.setGreetingService(new TestGreetingService());
    }

    @Test
    void setGreetingService() {
        String result = controller.sayHello();
        System.out.println(result);
        assert "Hello world".equals(result);
    }
}