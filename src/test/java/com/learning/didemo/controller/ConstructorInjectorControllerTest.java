package com.learning.didemo.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ConstructorInjectorControllerTest {

    private ConstructorInjectorController controller;

    @BeforeEach
    void setUp() {
//        controller = new ConstructorInjectorController(new GreetingServiceImpl());
    }

    @Test
    void sayHello() {
        String result = controller.sayHello();
        System.out.println(result);
        assert "Hello world".equals(result);
    }
}