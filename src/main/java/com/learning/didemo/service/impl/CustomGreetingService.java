package com.learning.didemo.service.impl;

import com.learning.didemo.service.GreetingService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile("qa")
@Service
public class CustomGreetingService implements GreetingService {
    @Override
    public String getGreeting() {
        return "Hello world";
    }
}
