package com.learning.didemo.service.impl;

import com.learning.didemo.service.GreetingService;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

@Primary
@Service
public class DummyGreetingService implements GreetingService {

    @Override
    public String getGreeting() {
        return "dummy";
    }
}
