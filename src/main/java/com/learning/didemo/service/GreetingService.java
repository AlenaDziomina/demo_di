package com.learning.didemo.service;

public interface GreetingService {

    String getGreeting();
}
