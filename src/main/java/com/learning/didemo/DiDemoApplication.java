package com.learning.didemo;

import com.learning.didemo.controller.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class DiDemoApplication {

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(DiDemoApplication.class, args);
        ConstructorInjectorController controller = ctx.getBean(SpecificProdConstructorInjectorController.class);
        String greeting = controller.sayHello();
        System.out.println(greeting);
    }

}
