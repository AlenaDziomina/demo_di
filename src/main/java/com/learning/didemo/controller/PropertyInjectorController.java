package com.learning.didemo.controller;

import com.learning.didemo.service.GreetingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

@Controller
public class PropertyInjectorController {

    @Autowired
    public GreetingService greetingService;

    public String sayHello() {
        return greetingService.getGreeting();
    }
}
