package com.learning.didemo.controller;

import org.springframework.stereotype.Controller;

@Controller
public class HelloWorldController {

    public String sayHello() {
        return "Hello";
    }
}
