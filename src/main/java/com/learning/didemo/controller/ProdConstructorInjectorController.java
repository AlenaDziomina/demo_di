package com.learning.didemo.controller;

import com.learning.didemo.service.AuthorService;
import com.learning.didemo.service.BookService;
import com.learning.didemo.service.GreetingService;
import com.learning.didemo.service.PublisherService;
import org.springframework.stereotype.Controller;

@Controller
public class ProdConstructorInjectorController extends ConstructorInjectorController{

    PropertyInjectorController controller;

    public ProdConstructorInjectorController(GreetingService greetingService,
                                             AuthorService authorService,
                                             PublisherService publisherService,
                                             HelloWorldController controller,
                                             PropertyInjectorController propertyInjectorController) {
        super(greetingService, authorService, publisherService, controller);
        this.controller = propertyInjectorController;
    }
}
