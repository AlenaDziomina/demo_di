package com.learning.didemo.controller;

import com.learning.didemo.service.AuthorService;
import com.learning.didemo.service.GreetingService;
import com.learning.didemo.service.PublisherService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

@RequiredArgsConstructor
@Controller
public class ConstructorInjectorController {

    private final GreetingService greetingService;
    private final AuthorService authorService;
    private final PublisherService publisherService;
    private final HelloWorldController controller;


    public String sayHello() {
        return greetingService.getGreeting();
    }
}
